#!/usr/bin/python

# TODO
# 	work from within scribus
#	work without fontforge requirements
#	python3
#	produce sample PDF with correct fonts
# USAGE : python checkGlyph.py glyph_unicode directory 
# EXAMPLE : python checkGlyph.py unid230 .
# EXAMPLE : python checkGlyph.py uni2009 /home/toto/fonts
#tifinagh : uni2d30
#fine insecable : uni2009 

import fontforge
import os, os.path, sys

ok=[]
fichier=[]

def listdirectory2(path): 
	""" 
	loop through directoriers to get the font file list
	
	Parameter :
		path : system path in which the font file will be searched
	
	Return :
		list of font file of type otf or ttf
	"""
    #fichier=[] 
    for root, dirs, files in os.walk(path): 
        for i in files: 
            fichier.append(os.path.join(root, i)) 
    return fichier

def check_glyph(directory):
	""" Check for glyph in fonts
	
	parameters :
		directory : directory in which the fonts will be taken
	return :
		print status of existing glyph in the current font
	"""
	
	fichier = listdirectory2(directory)
	for i in fichier:
		if i.split('.')[-1] == "ttf" or i.split('.')[-1] == "otf":
			#print "///"
			try:
				#print "***"
				font = fontforge.open(i)
				#print "---------- "+i+"----------"
				car = sys.argv[1]
				car_up = car.upper()
				#print car_up
				try:
					if font.selection.select("u+"+car):
						#print "hello "
						print font["u+"+car_up] #caps necesarily
						ok.append(i)
					else :
						pass #print "none selected 1"
				except TypeError :
					pass #print "none with exception sur : "+i
				font.close()
			except:
				pass


check_glyph(sys.argv[2])


#check_glyph("/home/cedric/Bureau/fontes/")
#check_glyph(os.path.expanduser("~")+"/.fonts")


print "---------------------------------"
print "check for "+sys.argv[1]
print "---------------------------------"
calcul = float(len(ok))/float(len(fichier))
#print "pourcentage de fontes couvrant "+sys.argv[1]+" : "+str(len(ok))+" sur "+ str(len(fichier))+" = "+str(calcul)+" %"
print "pourcentage de fontes couvrant %s : %i sur %i = %.2f %%" % (sys.argv[1],len(ok),len(fichier),calcul)


for fontes in ok:
	print fontes

		

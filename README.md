# Check4language

Many fonts just don’t cover many languages. These script aims to find which font covers a specific language.


## requirements :
python
fontforge with python support

## usage :
python check.py directory glyph

- *directory* is the path containing fonts we want to check (all in one directory : can be system dir, user dir, or any other)
- *glyph* is an a unicode identifier for a typical glyph for the language
